export let validator = {
    kiemTraRong: function (valueInput, idError, message) {
      if (valueInput == "") {
        document.getElementById(idError).innerText = message;
        document.getElementById(idError).style.display = "block";
        return false;
      } else {
        document.getElementById(idError).innerText = "";
        document.getElementById(idError).style.display = "none";
        return true;
      }
    },
    kiemTraChuoiSo: function (valueInput, idError) {
      var regex = /^[0-9]+$/;
  
      if (regex.test(valueInput)) {
        document.getElementById(idError).innerText = "";
        document.getElementById(idError).style.display = "none";
        return true;
      } else {
        document.getElementById(idError).innerText =
          "Trường này chỉ được nhập số";
        document.getElementById(idError).style.display = "block";
        return false;
      }
    },
}