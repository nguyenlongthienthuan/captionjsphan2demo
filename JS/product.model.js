export class products {
  constructor(id,
    name,
    price,
    type,
    img,
    screen,
    backCamera,
    frontCamera,
    desc,
   ) {
    this.id=id
    this.name = name
    this.price = price
    this.type = type
    this.img = img
    this.screen=screen
    this.backCamera=backCamera
    this.frontCamera=frontCamera
    this.desc=desc
  }
}
